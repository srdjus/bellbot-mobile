import 'package:flutter/material.dart';
import 'package:iotgate/models/session.dart';
import 'package:iotgate/screens/history.dart';
import 'package:iotgate/screens/register.dart';
import 'package:provider/provider.dart';
import 'package:iotgate/screens/home.dart';
import 'package:iotgate/screens/login.dart';
import 'package:iotgate/screens/settings.dart';
import 'package:iotgate/screens/monitoring.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => SessionModel(),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gate',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        fontFamily: 'OpenSans',
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: 'home',
      routes: {
        'home': (context) => const HomeScreen(),
        'settings': (context) => const SettingsScreen(),
        'monitoring': (context) => const MonitoringScreen(),
        'history': (context) => const HistoryScreen(),
        'login': (context) => const LoginScreen(title: 'Login'),
        'register': (context) => const RegisterScreen()
      },
    );
  }
}
