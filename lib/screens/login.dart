import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:iotgate/components/navigation_drawer.dart';
import 'package:iotgate/models/session.dart';
import 'package:provider/provider.dart';

import 'package:iotgate/helpers/constants.dart' as Constants;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  // TODO: Link obscure text with a button
  // Hide password
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff111111),
            Color(0xff222222),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        drawer: const NavigationDrawer(),
        body: Container(
          padding: const EdgeInsets.all(8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.circle_outlined,
                    color: Colors.white,
                    size: 25,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Dobrodošli u Bellbot',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w100,
                        letterSpacing: -0.5),
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Center(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: _usernameController,
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w100),
                        decoration: const InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.6),
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.white,
                            ),
                          ),
                          filled: true,
                          fillColor: Colors.transparent,
                          prefixIcon: Icon(
                            Icons.person_rounded,
                            color: Colors.white,
                          ),
                          labelStyle: TextStyle(
                            color: Colors.white,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.white,
                          ),
                          labelText: "Korisničko ime",
                          hintText: "Unesite korisničko ime",
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        controller: _passwordController,
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w100),
                        decoration: const InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.6),
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.white,
                            ),
                          ),
                          filled: true,
                          fillColor: Colors.transparent,
                          prefixIcon: Icon(
                            Icons.key,
                            color: Colors.white,
                          ),
                          labelStyle: TextStyle(
                            color: Colors.white,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.white,
                          ),
                          labelText: "Loznika",
                          hintText: "Unesite lozinku",
                        ),
                        obscureText: obscureText,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                          minimumSize: MaterialStateProperty.all(
                            const Size(200, 50),
                          ),
                          backgroundColor: MaterialStateProperty.all(
                            Colors.white,
                          ),
                        ),
                        onPressed: _submitForm,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                "Potvrdite",
                                style: TextStyle(
                                  letterSpacing: -1,
                                  color: Colors.black87,
                                  fontSize: 20,
                                ),
                              ),
                              Icon(
                                Icons.arrow_forward,
                                color: Colors.black87,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitForm() async {
    try {
      final http.Response response = await http.post(
        Uri.http(
          Constants.apiHostName,
          '/token/',
        ),
        body: <String, String>{
          'username': _usernameController.text,
          'password': _passwordController.text,
        },
      );

      if (response.statusCode == 200) {
        // Logged in (saving the access token to the storage)
        const storage = FlutterSecureStorage();

        final responseData = jsonDecode(response.body);

        print("Token value: ${responseData['access_token']}");

        await storage.write(
          key: 'access_token',
          value: responseData['access_token'],
        );

        _proceed();
      } else {
        // TODO: Handle error status codes
        print("There was an error status code: ${response.statusCode}");
      }
    } catch (err) {
      // TODO: Handle error
      print(err);
    }
  }

  void _proceed() {
    Provider.of<SessionModel>(
      context,
      listen: false,
      // TODO: Send actual data
    ).login({});

    // Redirect
    Navigator.pushReplacementNamed(context, 'settings');
  }
}
