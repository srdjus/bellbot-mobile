import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:iotgate/models/session.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // TODO: Find a better way to do this
  @override
  void initState() {
    super.initState();

    final user = Provider.of<SessionModel>(context, listen: false).user;

    SchedulerBinding.instance.addPostFrameCallback((_) {
      // TODO: Screen transition
      Future.delayed(const Duration(seconds: 2), () {
        if (user.isNotEmpty) {
          Navigator.pushReplacementNamed(context, 'settings');
        } else {
          Navigator.pushReplacementNamed(context, 'login');
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff111111),
            Color(0xff222222),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.circle_outlined,
                size: 50.0,
                color: Colors.white,
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Bellbot',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 20.0,
                  ),
                ),
              ),
              const Text(
                'Automated gate\'s system.',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w100,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
