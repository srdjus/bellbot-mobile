import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:iotgate/components/navigation_drawer.dart';
import 'package:http/http.dart' as http;

import 'package:iotgate/helpers/constants.dart' as constants;
import 'package:iotgate/models/session.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  Timer? timer;

  final _formKey = GlobalKey<FormState>();
  final _plateNumberFieldKey = GlobalKey<FormFieldState>();

  final _plateNumberController = TextEditingController();
  final _plateDescriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff111111),
            Color(0xff222222),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: const Text('Panel'),
          foregroundColor: Colors.white,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        drawer: const NavigationDrawer(),
        body: Container(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: [
              const SizedBox(
                height: 5,
              ),
              Material(
                shadowColor: Colors.black,
                elevation: 5,
                borderRadius: const BorderRadius.all(Radius.circular(5)),
                child: ExpansionTile(
                  initiallyExpanded: false,
                  title: const Text('Konfigurišite registarske oznake'),
                  subtitle:
                      const Text('Dodavanje, aktiviranje i deaktiviranje'),
                  children: [
                    ListTile(
                      onTap: () {
                        showModalBottomSheet<void>(
                          isScrollControlled: true,
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          )),
                          context: context,
                          builder: (BuildContext context) {
                            return Container(
                              // For keyboard
                              padding: MediaQuery.of(context).viewInsets,
                              color: Colors.transparent,
                              child: Container(
                                padding: const EdgeInsets.all(10),
                                decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                ),
                                child: Form(
                                  key: _formKey,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.add_circle_outline,
                                            color: Colors.blueAccent,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          const Text(
                                            'Nove tablice',
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                              letterSpacing: -1,
                                              color: Color(0xff555555),
                                            ),
                                          ),
                                          const Spacer(),
                                          TextButton(
                                            onPressed: () {
                                              if (_formKey.currentState
                                                      ?.validate() ==
                                                  true) {
                                                _submitNewPlate();
                                              }
                                            },
                                            child: Row(children: [
                                              const Icon(
                                                Icons.save,
                                                color: Colors.blueAccent,
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                'Sačuvajte'.toUpperCase(),
                                                style: const TextStyle(
                                                  color: Colors.blueAccent,
                                                ),
                                              ),
                                            ]),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      TextFormField(
                                        key: _plateNumberFieldKey,
                                        controller: _plateNumberController,
                                        decoration: const InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color:
                                                  Color.fromRGBO(0, 0, 0, 0.2),
                                              width: 1,
                                            ),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              width: 1,
                                              color:
                                                  Color.fromRGBO(0, 0, 0, 0.5),
                                            ),
                                          ),
                                          filled: true,
                                          fillColor: Colors.transparent,
                                          prefixIcon: Icon(
                                            Icons.numbers,
                                            color: Color(0xff555555),
                                          ),
                                          labelStyle: TextStyle(
                                            color: Colors.black,
                                          ),
                                          hintStyle: TextStyle(
                                            color: Colors.black,
                                          ),
                                          labelText: 'Oznaka (npr. A12-A-345)',
                                          hintText: 'Unesite oznaku',
                                        ),
                                        onChanged: (String? value) {
                                          _plateNumberFieldKey.currentState
                                              ?.validate();
                                        },
                                        validator: (String? value) {
                                          if (!RegExp(
                                                  r"^[AEJKMOT0-9][0-9]{2}-[AEJKMOT]-[0-9]{3}$")
                                              .hasMatch(value!)) {
                                            return "Registarske oznake moraju da budu u tačnom formatu.";
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                      leading: const Icon(
                        Icons.add_circle_outline,
                        color: Colors.blueAccent,
                      ),
                      title: const Text('Dodajte tablice'),
                    ),
                    FutureBuilder<http.Response>(
                      future: _fetchPlates(),
                      builder: (BuildContext context, snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data!.statusCode == 200) {
                            // We have data
                            List plates = jsonDecode(
                              snapshot.data!.body,
                            );
                            return ListView.builder(
                              shrinkWrap: true,
                              itemCount: plates.length,
                              scrollDirection: Axis.vertical,
                              physics: const NeverScrollableScrollPhysics(),
                              controller: ScrollController(),
                              itemBuilder: (BuildContext context, i) {
                                return ListTile(
                                  leading: plates[i]['is_active']
                                      ? const Icon(
                                          Icons.check_circle_outline,
                                          color: Colors.green,
                                        )
                                      : const Icon(
                                          Icons.error_outline,
                                          color: Colors.red,
                                        ),
                                  title: Text(plates[i]['number']),
                                  subtitle: Text(
                                      '${plates[i]["owner"]["username"]} (${plates[i]["owner"]["email"]})'),
                                  trailing: _buildControls(plates[i]),
                                );
                              },
                            );
                            // return _buildPlateList(platesTemp);
                          }
                        }

                        return const Text('Ništa za prikazati.');
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildControls(Map plate) {
    return Consumer<SessionModel>(
      builder: (context, session, child) {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (session.user['is_admin'] && !plate['is_active'])
              IconButton(
                onPressed: () => _updatePlate(plate['id'], true),
                icon: const Icon(
                  Icons.check,
                  color: Colors.green,
                ),
              ),
            if (session.user['is_admin'] && plate['is_active'])
              IconButton(
                onPressed: () => _updatePlate(plate['id'], false),
                icon: const Icon(
                  Icons.close,
                  color: Colors.redAccent,
                ),
              )
          ],
        );
      },
    );
  }

  void _submitNewPlate() async {
    const storage = FlutterSecureStorage();
    String? token = await storage.read(key: 'access_token');

    if (token == null) return;
    try {
      final http.Response response = await http.post(
        Uri.http(constants.apiHostName, '/plates/'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        },
        body: jsonEncode(
          <String, String>{
            'number': _plateNumberController.text,
            if (_plateDescriptionController.text.isNotEmpty)
              'description': _plateDescriptionController.text,
          },
        ),
      );

      if (response.statusCode == 200) {
        if (!mounted) return;
        Navigator.pop(context);

        // Updating data
        setState(() {});
      } else {
        // Handle error status codes
        print('Got error: ${response.statusCode}');
      }
    } catch (err) {
      // TODO: Handle error
      print(err);
    }
  }

  Future<http.Response> _fetchPlates() async {
    const storage = FlutterSecureStorage();
    String? token = await storage.read(key: 'access_token');

    return http.get(
      Uri.http(constants.apiHostName, '/plates/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
    );
  }

  void _updatePlate(int plateId, bool active) async {
    const storage = FlutterSecureStorage();
    String? token = await storage.read(key: 'access_token');

    try {
      final http.Response response = await http.put(
        Uri.http(constants.apiHostName, '/plates/$plateId/activate/'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode(<String, dynamic>{'value': active}),
      );

      if (response.statusCode == 200) {
        // Deleted, update screen
        setState(() {});
      } else {
        // TODO: Handle error status codes
        print(response);
      }
    } catch (err) {
      // TODO: Handle error
      print(err);
    }
  }

  @override
  void dispose() {
    timer?.cancel();

    super.dispose();
  }
}
