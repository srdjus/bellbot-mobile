import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:iotgate/components/navigation_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:iotgate/helpers/constants.dart' as constants;

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({super.key});

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff111111),
            Color(0xff222222),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: const Text('Istorija'),
          foregroundColor: Colors.white,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        drawer: const NavigationDrawer(),
        body: Container(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: [
              const Text(
                'Zatvoreni prolazi',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              FutureBuilder<http.Response>(
                future: _fetchEntries(),
                builder: (BuildContext context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data!.statusCode == 200) {
                      // We have data
                      List entries = jsonDecode(
                        snapshot.data!.body,
                      );
                      return ListView.builder(
                        shrinkWrap: true,
                        itemCount: entries.length,
                        scrollDirection: Axis.vertical,
                        physics: const NeverScrollableScrollPhysics(),
                        controller: ScrollController(),
                        itemBuilder: (BuildContext context, i) {
                          return CustomListItem(
                            plateNumber: entries[i]['plate']['number'],
                            username: entries[i]['plate']['owner']['username'],
                            email: entries[i]['plate']['owner']['email'],
                            datetimeInRaw: entries[i]['datetime_in'],
                            datetimeOutRaw: entries[i]['datetime_out'],
                          );
                        },
                      );
                      // return _buildPlateList(platesTemp);
                    }
                  }

                  return const Text('Ništa za prikazati.');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<http.Response> _fetchEntries() async {
    const storage = FlutterSecureStorage();
    String? token = await storage.read(key: 'access_token');

    return http.get(
      Uri.http(
        constants.apiHostName,
        '/entries/',
      ),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
    );
  }
}

class CustomListItem extends StatelessWidget {
  const CustomListItem({
    super.key,
    required this.plateNumber,
    required this.username,
    required this.email,
    required this.datetimeInRaw,
    required this.datetimeOutRaw,
  });

  final String plateNumber;
  final String username;
  final String email;
  final String datetimeInRaw;
  final String? datetimeOutRaw;

  @override
  Widget build(BuildContext context) {
    DateTime datetimeIn = DateTime.parse(datetimeInRaw);
    DateTime? datetimeOut;

    if (datetimeOutRaw != null) {
      datetimeOut = DateTime.parse(datetimeOutRaw!);
    }

    final dateFormat = DateFormat('dd.MM.yyyy. HH:mm:ss');

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      margin: const EdgeInsets.only(bottom: 10),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(5),
          topRight: Radius.circular(10),
          bottomRight: Radius.circular(5),
          bottomLeft: Radius.circular(10),
        ),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(200, 200, 200, 0.2),
            blurRadius: 3,
            spreadRadius: 2,
            offset: Offset(1, 1), // Shadow position
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  width: 1,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    const Icon(Icons.time_to_leave),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      plateNumber,
                      style: const TextStyle(
                        fontSize: 20.0,
                        letterSpacing: -1,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    const Icon(
                      Icons.camera_outdoor,
                      size: 25.0,
                      color: Colors.black54,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Text(
                          dateFormat.format(datetimeIn),
                          style: const TextStyle(
                              color: Colors.black87,
                              letterSpacing: -0.5,
                              fontWeight: FontWeight.w400),
                        ),
                        if (datetimeOut != null)
                          Text(
                            dateFormat.format(datetimeOut),
                            style: const TextStyle(
                                color: Colors.black87,
                                letterSpacing: -0.5,
                                fontWeight: FontWeight.w400),
                          ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 20),
            child: Text('Korisničko ime: $username'),
          ),
          Container(
            margin: const EdgeInsets.only(left: 20),
            child: Text('E-mail: $email'),
          ),
        ],
      ),
    );
  }
}
