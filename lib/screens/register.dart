import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:iotgate/components/navigation_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:iotgate/models/session.dart';
import 'package:provider/provider.dart';

import 'package:iotgate/helpers/constants.dart' as Constants;

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final _usernameFieldKey = GlobalKey<FormFieldState>();
  final _emailFieldKey = GlobalKey<FormFieldState>();
  final _passwordFieldKey = GlobalKey<FormFieldState>();

  final _emailController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _usernameAvailable = true;
  bool _emailAvailable = true;
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff111111),
            Color(0xff222222),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        drawer: const NavigationDrawer(),
        body: Container(
          padding: const EdgeInsets.all(8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.circle_outlined,
                    color: Colors.white,
                    size: 25,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Registracija',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w100,
                        letterSpacing: -0.5),
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Center(
                child: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    children: [
                      TextFormField(
                        key: _emailFieldKey,
                        controller: _emailController,
                        autovalidateMode: AutovalidateMode.disabled,
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w100),
                        decoration: const InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.6),
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.white,
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.redAccent,
                            ),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.orangeAccent,
                            ),
                          ),
                          filled: true,
                          fillColor: Colors.transparent,
                          prefixIcon: Icon(
                            Icons.mail,
                            color: Colors.white,
                          ),
                          labelStyle: TextStyle(
                            color: Colors.white,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.white,
                          ),
                          labelText: 'E-mail',
                          hintText: 'Unesite e-mail',
                        ),
                        onChanged: (String? value) {
                          setState((() {
                            // This is reset after every input
                            // (availability is checkd on button pressed)
                            _emailAvailable = true;
                            _emailFieldKey.currentState?.validate();
                          }));
                        },
                        validator: (String? value) {
                          if (value?.contains("@") == false) {
                            return "Neispravan e-mail.";
                          }

                          return null;
                        },
                      ),
                      if (_emailFieldKey.currentState != null &&
                          _emailFieldKey.currentState!.isValid &&
                          !_emailAvailable)
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: const [
                              Icon(
                                Icons.error_outline,
                                color: Colors.redAccent,
                                size: 15,
                              ),
                              Text(
                                'ZAUZET',
                                style: TextStyle(
                                  color: Colors.redAccent,
                                  fontSize: 10,
                                ),
                              ),
                            ],
                          ),
                        ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        key: _usernameFieldKey,
                        controller: _usernameController,
                        autovalidateMode: AutovalidateMode.disabled,
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w100,
                        ),
                        decoration: const InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.6),
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.white,
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.redAccent,
                            ),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.orangeAccent,
                            ),
                          ),
                          filled: true,
                          fillColor: Colors.transparent,
                          prefixIcon: Icon(
                            Icons.person_rounded,
                            color: Colors.white,
                          ),
                          labelStyle: TextStyle(
                            color: Colors.white,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.white,
                          ),
                          labelText: "Korisničko ime",
                          hintText: "Unesite korisničko ime",
                        ),
                        onChanged: (String? value) {
                          setState(() {
                            _usernameAvailable = true;
                            _usernameFieldKey.currentState?.validate();
                          });
                        },
                        validator: (String? value) {
                          if (value!.length < 3 || value.length > 10) {
                            return "Između 3 i 10 znakova.";
                          }

                          if (!RegExp(r'^[a-z0-9]+$').hasMatch(value)) {
                            return "Mala slova, samo alfanumerički znakovi.";
                          }

                          return null;
                        },
                      ),
                      if (_usernameFieldKey.currentState != null &&
                          _usernameFieldKey.currentState!.isValid &&
                          !_usernameAvailable)
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: const [
                              Icon(
                                Icons.error_outline_outlined,
                                color: Colors.redAccent,
                                size: 15,
                              ),
                              Text(
                                'ZAUZET',
                                style: TextStyle(
                                  color: Colors.redAccent,
                                  fontSize: 10,
                                ),
                              ),
                            ],
                          ),
                        ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        key: _passwordFieldKey,
                        controller: _passwordController,
                        autovalidateMode: AutovalidateMode.disabled,
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w100),
                        decoration: InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromRGBO(255, 255, 255, 0.6),
                              width: 1,
                            ),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.white,
                            ),
                          ),
                          errorBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.redAccent,
                            ),
                          ),
                          focusedErrorBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.orangeAccent,
                            ),
                          ),
                          filled: true,
                          fillColor: Colors.transparent,
                          prefixIcon: const Icon(
                            Icons.key,
                            color: Colors.white,
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                obscureText = !obscureText;
                              });
                            },
                            icon: const Icon(
                              Icons.visibility_outlined,
                              color: Colors.white,
                            ),
                          ),
                          labelStyle: const TextStyle(
                            color: Colors.white,
                          ),
                          hintStyle: const TextStyle(
                            color: Colors.white,
                          ),
                          labelText: "Lozinka",
                          hintText: "Unesite lozinku",
                        ),
                        obscureText: obscureText,
                        onChanged: (String? value) {
                          setState(() {
                            _passwordFieldKey.currentState?.validate();
                          });
                        },
                        validator: (String? value) {
                          if (value!.length < 3) {
                            return "Lozinka treba da sadrži najmanje 3 znaka.";
                          }

                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                          minimumSize: MaterialStateProperty.all(
                            const Size(200, 50),
                          ),
                          backgroundColor: MaterialStateProperty.all(
                            Colors.white,
                          ),
                        ),
                        onPressed: () {
                          if (_formKey.currentState?.validate() == true) {
                            _submitForm();
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                "Pridružite se",
                                style: TextStyle(
                                  letterSpacing: -1,
                                  color: Colors.black87,
                                  fontSize: 20,
                                ),
                              ),
                              Icon(
                                Icons.arrow_forward,
                                color: Colors.black87,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitForm() async {
    try {
      final http.Response response = await http.post(
        Uri.http(
          Constants.apiHostName,
          '/users/',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'email': _emailController.text,
          'username': _usernameController.text,
          'password': _passwordController.text,
        }),
      );

      if (response.statusCode == 200) {
        // We're not signing in, just redirecting
        _proceed();
      } else if (response.statusCode == 400) {
        final responseData = jsonDecode(response.body);
        if (responseData["detail"] == "E-mail already registered") {
          setState(
            () => _emailAvailable = false,
          );
        }

        if (responseData["detail"] == "Username already registered") {
          setState(
            () => _usernameAvailable = false,
          );
        }
      }
    } catch (err) {
      // TODO: Handle error
      print(err);
    }
  }

  void _proceed() {
    Provider.of<SessionModel>(
      context,
      listen: false,
      // TODO: Send actual data
    ).login({});

    // Redirect
    Navigator.pushReplacementNamed(context, 'login');
  }
}
