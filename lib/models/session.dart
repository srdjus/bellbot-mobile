import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import 'package:iotgate/helpers/constants.dart' as constants;

class SessionModel extends ChangeNotifier {
  SessionModel() : super() {
    // Running to see if there is a session
    print('Checking session...');
    _checkSession();
  }

  // Storing user (if it's empty user is not logged in)
  Map _user = {};

  Map get user => _user;

  void login(Map u) {
    // TODO: Fix this to receive user data together with token so they can be logged in without checking
    _checkSession();
    notifyListeners();
  }

  void logout() {
    _user.clear();
  }

  void _checkSession() async {
    const storage = FlutterSecureStorage();

    String? token = await storage.read(key: 'access_token');

    if (token == null) return;

    try {
      // Get data using the token
      final http.Response response = await http.get(
        Uri.http(constants.apiHostName, '/users/me/'),
        headers: {
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        final userData = jsonDecode(response.body);

        _user['username'] = userData['username'];
        _user['is_admin'] = userData['is_admin'];
      } else {
        // TODO: Handle error status codes
        print("There was an error status code: ${response.statusCode}");
      }
    } catch (err) {
      // TODO: Handle error
      print(err);
    }
  }
}
