import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:iotgate/models/session.dart';
import 'package:provider/provider.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Consumer<SessionModel>(
        builder: (context, session, child) => ListView(
          children: [
            if (session.user.isNotEmpty)
              ListTile(
                onTap: () => Navigator.pushNamed(context, 'settings'),
                leading: const Icon(
                  Icons.settings,
                  color: Colors.red,
                ),
                title: const Text(
                  "Podešavanja",
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
              ),
            if (session.user.isNotEmpty && session.user['is_admin'])
              ListTile(
                onTap: () => Navigator.pushNamed(context, 'monitoring'),
                leading: const Icon(
                  Icons.monitor,
                  color: Colors.red,
                ),
                title: const Text(
                  "Monitoring",
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
              ),
            if (session.user.isNotEmpty)
              ListTile(
                onTap: () => Navigator.pushNamed(context, 'history'),
                leading: const Icon(
                  Icons.history,
                  color: Colors.red,
                ),
                title: const Text(
                  "Istorija",
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
              ),
            if (session.user.isEmpty)
              if (session.user.isEmpty)
                ListTile(
                  onTap: () => Navigator.pushNamed(context, 'login'),
                  leading: const Icon(
                    Icons.arrow_circle_right_outlined,
                    color: Colors.red,
                  ),
                  title: const Text(
                    'Sign in',
                    style: TextStyle(
                      color: Colors.black87,
                    ),
                  ),
                ),
            if (session.user.isEmpty)
              ListTile(
                onTap: () => Navigator.pushNamed(context, 'register'),
                leading: const Icon(
                  Icons.add_circle_outline,
                  color: Colors.red,
                ),
                title: const Text(
                  'Register',
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
              ),
            ListTile(
              onTap: () {},
              leading: const Icon(
                Icons.question_mark,
                color: Colors.red,
              ),
              title: const Text(
                "O aplikaciji",
                style: TextStyle(
                  color: Colors.black87,
                ),
              ),
            ),
            if (session.user.isNotEmpty)
              ListTile(
                onTap: () => _signOut(context),
                leading: const Icon(
                  Icons.exit_to_app,
                  color: Colors.red,
                ),
                title: const Text(
                  "Odjavite se",
                  style: TextStyle(),
                ),
              ),
          ],
        ),
      ),
    );
  }

  void _deleteToken() async {
    const storage = FlutterSecureStorage();
    await storage.delete(key: 'access_token');
  }

  void _signOut(BuildContext context) {
    _deleteToken();

    Provider.of<SessionModel>(
      context,
      listen: false,
    ).logout();

    Navigator.pushReplacementNamed(context, 'login');
  }
}
